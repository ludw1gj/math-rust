pub fn round_float(n: f64, d_points: usize) -> String {
  let rounder = (10 ^ d_points) as f64;
  let answer = (n * rounder).round() / rounder;
  return format!("{:.*}", d_points, answer);
}

#[allow(dead_code)]
fn build_str() -> String {
  let mut s = String::new();
  s.push_str("Hello, world!");
  return s;
}
