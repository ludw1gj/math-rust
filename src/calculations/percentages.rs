use calculations::util::round_float;

pub fn change_by_percentage(n: f64, percent: f64) -> String {
  /*
		ChangeByPercentage:
		Increase/Decrease a number by a percentage.
		(n / 100) * p + n

		First work out 1% of 250, 250 ÷ 100 = 2.5 then multiply the answer by 23,
		because there was a 23% increase. 2.5 × 23 = 57.5.
	*/
  let one_percent: f64 = n / 100.0;
  let addition_value: f64 = one_percent * percent;
  let answer = n + addition_value;

  let noun = || -> String {
    if percent < 0.0 {
      return "decrease".to_string();
    } else {
      return "increase".to_string();
    }
  }();

  return json!({
      "one_percent": round_float(one_percent, 2),
      "addition_value": round_float(addition_value, 2),
      "answer": round_float(answer, 2),
      "noun": noun
    }).to_string();
}

pub fn find_percentage_value(p: f64, n: f64) -> String {
  /*
  	Find the value of the percentage of a number.
  	(p / 100) * n
  */
  let divided = p / 100.0;
  let answer = divided * n;

  return json!({
    "percentage": round_float(p, 2),
    "number": round_float(n, 2),
    "divided": round_float(divided, 2),
    "answer": round_float(answer, 2)
  }).to_string();
}

pub fn find_percentage_difference(n1: f64, n2: f64) -> String {
  /*
		Find the percentage change from one number to another.
		(new_n - original_n) / original_n * 100
	*/
  let difference = n2 - n1;
  let percentage_d = difference / n1;
  let answer = percentage_d * 100.0;

  return json!({
    "n1": round_float(n1, 2),            
		"n2": round_float(n2, 2)    ,         
		"difference": round_float(difference, 2)   ,             
		"percentage_d": round_float(percentage_d, 2) ,
		"answer": round_float(answer, 2)     ,           
  }).to_string();
}

pub fn find_percentage(n1: f64, n2: f64) -> String {
  /*
		Find the percentage of a number of a total number.
		(n / total_n) * 100
	*/
  let percentage_d = n1 / n2;
  let answer = percentage_d * 100.0;
  return json!({
    "n1": n1,
    "n2": n2,
    "percentage_d": percentage_d,
    "answer": answer
  }).to_string();
}
