use std;
use calculations::util::round_float;

fn multiply(a: i32, b: i32) {
  a * b;
}

// Outputs the proof and answer of finding the hypotenuse using the pythagorean theorem.
pub fn pythagorean_theorem(a: f64, b: f64) -> String {
  /*
		Use the Pythagorean Theorem to find side lengths (a or b), or the
		hypotenuse (c) of a right-angle triangle.

		Pythagorean Theorem: right-angle triangle with side lengths (a, b) and hypotenuse (c).
		a^2 + b^2 = c^2

		Example:
		When a = 25 and b = 17, find c.
		c^2 	= 25^2 + 17^2
			= 625 + 289
			= 914
		c	= √914
			≈ 30.2
	*/
  let a_sqr = a.powf(2.0);
  let b_sqr = b.powf(2.0);
  let ab = a_sqr + b_sqr;
  let answer = ab.powf(2.0);

  return json!({
      "a_sqr": round_float(a_sqr, 2),
      "b_sqr": round_float(b_sqr, 2),
      "ab": round_float(ab, 2),
      "answer": round_float(answer, 2)
    }).to_string();
}

pub fn tsa_cone(radius: f64, slant_height: f64) -> String {
  /*
		Find the Total Surface Area of a cone, with the equation:
		Where S is Slant Height, r is radius
		TSA     = tsa of base (circle) + tsa of curved surface
			= πr^2 + πrS
			= πr(r + S)
	*/
  let base_area = std::f64::consts::PI * radius.powf(2.0);
  let curved_surface_area = std::f64::consts::PI * radius * slant_height;
  let tsa = base_area + curved_surface_area;

  return json!({
      "radius": round_float(radius, 2),
      "slant_height": round_float(slant_height, 2),
      "base_area": round_float(base_area, 2),
      "curved_surface_area": round_float(curved_surface_area, 2),
      "answer": round_float(tsa, 2)
    }).to_string();
}

// Outputs the proof and answer of finding the total surface area of a cube.
pub fn tsa_cube(length: f64) -> String {
  /*
		Find the Total Surface Area of a cube, with the equation:
		TSA = 6L^2
	*/
  let length_sqr = length.powf(2.0);
  let tsa = 6.0 * length_sqr;

  return json!({
    "length": round_float(length, 2),
    "length_sqr": round_float(length_sqr, 2),
    "answer": round_float(tsa, 2)
  }).to_string();
}

pub fn tsa_cylinder(radius: f64, height: f64) -> String {
  /*
		Find the Total Surface Area of a cylinder, with the equation:
		TSA = tsa of 2 circles + curved surface
		    = 2πr^2 + 2πrh
		    = 2πr(r + h)
	*/
  let radius_sqr = radius.powf(2.0);
  let circle_area = 2.0 * std::f64::consts::PI * radius_sqr;
  let rh = radius * height;
  let curved_surface_area = 2.0 * std::f64::consts::PI * rh;
  let tsa = circle_area * curved_surface_area;

  return json!({
    "radius": round_float(radius, 2),
    "height": round_float(height, 2),
    "rh": round_float(rh, 2),
    "curved_surface_area": round_float(curved_surface_area, 2),
    "tsa": round_float(tsa, 2)
  }).to_string();
}
