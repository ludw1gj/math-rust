#[macro_use]
extern crate serde_json;

mod calculations;

use calculations::percentages;

fn main() {
    println!(
        "{}\n{}\n{}\n{}",
        percentages::change_by_percentage(100.47, 25.0),
        percentages::find_percentage_value(50.0, 30.0),
        percentages::find_percentage_difference(25.0, 40.0),
        percentages::find_percentage(25.0, 40.0),
    );
}
